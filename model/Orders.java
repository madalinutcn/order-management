package model;

/**
 * Cu ajutorul acestei clase vor fi create obiectele de tipul Orders, campurile
 * acestei clase corespund campurilor tabelei orders din baza de date, acestea
 * fiind: id comenzii plasate de catre client care este unic si id clientului
 * care plaseaza comanda, care este si el unic.
 */

public class Orders {
	private int id;
	private int client_id;

	public Orders(int a, int b) {
		this.id = a;
		this.client_id = b;
	}

	public int getId() {
		return this.id;
	}

	public int getClientId() {
		return this.client_id;
	}

	public void setId(int a) {
		this.id = a;

	}

	public void setClientId(int a) {
		this.client_id = a;
	}

}
