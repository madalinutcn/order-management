package model;

/**
 * Cu ajutorul acestei clase vor fi create obiectele de tipul OrderItem,
 * campurile acestei clase corespund campurilor tabelei ordersitem din baza de
 * date, acestea fiind: un id unic pentru fiecare comanda, id comenzii ce face
 * referire la id clientului care este iar unic, id produsul ce este iar unic si
 * cantitatea ce se doreste a fi cumparata.
 */

public class OrderItem {
	private int id;
	private int orderId;
	private int productId;
	private int quantity;

	public OrderItem(int a, int b, int c, int d) {
		this.id = a;
		this.orderId = b;
		this.productId = c;
		this.quantity = d;
	}

	public int getId() {
		return this.id;

	}

	public int getOrderId() {
		return this.orderId;

	}

	public int getProductId() {
		return this.productId;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setId(int a) {
		this.id = a;
	}

	public void setOrderId(int a) {
		this.orderId = a;
	}

	public void setProductId(int a) {
		this.productId = a;
	}

	public void setQuantity(int a) {
		this.productId = a;
	}
}
