package model;

/**
 * Cu ajutorul acestei clase vor fi create obiectele de tipul Product, campurile
 * acestei clase corespund campurilor tabelei product din baza de date, acestea
 * fiind: un id unic pentru fiecare produs, un nume, cate produse de acest fel
 * avem pe stoc si pretul produsului.
 */

public class Product {
	private int id;
	private String name;
	private int quantity;
	private int price;

	public Product(int a, String b, int c, int d) {
		this.id = a;
		this.name = b;
		this.quantity = c;
		this.price = d;
	}

	public int getId() {
		return this.id;

	}

	public String getName() {
		return this.name;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public int getPrice() {
		return this.price;
	}

	public void setId(int a) {
		this.id = a;
	}

	public void setName(String a) {
		this.name = a;
	}

	public void setQuantity(int a) {
		this.quantity = a;
	}

	public void setPrice(int a) {
		this.price = a;
	}

}
