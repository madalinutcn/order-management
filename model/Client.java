package model;

/**Cu ajutorul acestei clase vor fi create obiectele de tipul Client, campurile acestei clase
 * corespund campurilor tabelei clients din baza de date, acestea fiind: un id unic ce identifica
 * fiecare client din baza de date, numele clientului, varsta acestuia si numarul de telefon
 * la care poate fi contactac pentru validarea comenzii.*/

public class Client {
	private int id;
	private String name;
	private int age;
	private String phone;

	public Client(int a, String b, int c, String d) {
		this.id = a;
		this.name = b;
		this.age = c;
		this.phone = d;
	}

	public int getId() {
		return this.id;

	}

	public String getName() {
		return this.name;
	}

	public int getAge() {
		return this.age;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setId(int a) {
		this.id = a;
	}

	public void setName(String a) {
		this.name = a;
	}

	public void setAge(int a) {
		this.age = a;
	}

	public void setPhone(String a) {
		this.phone = a;
	}

}
