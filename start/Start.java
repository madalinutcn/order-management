package start;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import presentation.GUI;
import presentation.GUI1;
import presentation.GUI2;

/**
 * Clasa principala de unde se porneste aplicatia propiu-zisa, de unde se poate
 * alege mai departe daca dorim sa efectuam o comanda, sa cream cate o factura
 * pentru fiecare comanda, sa efectuam operatii pe clienti sau sa efectuam
 * operatii pe produse.
 */

public class Start {

	public static void main(String[] args) {
		JFrame frame = new JFrame("WareHouse");
		JPanel panel = new JPanel();
		JButton buton1 = new JButton("Operations clients");
		JButton buton2 = new JButton("Operations products");
		JButton buton3 = new JButton("Create order/bills for each order");
		panel.add(buton1);
		panel.add(buton2);
		panel.add(buton3);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(3, 1));
		frame.setSize(250, 150);
		frame.setContentPane(panel);
		frame.setVisible(true);
		buton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GUI();

			}
		});
		buton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GUI1();

			}
		});
		buton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GUI2();

			}
		});
	}

}
