package presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLayer.LOGG;
import dataAccesLayer.ConnectionFactory;

import java.util.logging.Level;

/**
 * In aceasta clasa este realizata interfata pentru aplicatia de adaugarea a
 * unei comenzi + crearea unei facturi pentru fiecare comanda in parte, aceste
 * facturi fiind salvate intr-un fisier txt numit biils.log.
 */

public class GUI2 extends JFrame {
	
	private static final long serialVersionUID = 1L;
	JLabel l1 = new JLabel("Client ID:");
	JTextField t1 = new JTextField("");
	JLabel l2 = new JLabel("Product ID:");
	JTextField t2 = new JTextField("");
	JLabel l3 = new JLabel("Quantity:");
	JTextField t3 = new JTextField("");
	JButton bb = new JButton("Order");
	JButton bbb = new JButton("Bil for each order");
	JPanel panel = new JPanel();

	public static final String insert1 = "INSERT INTO orders (client_id) \r\n" + "values\r\n" + "(?);";
	public static final String query1 = "select MAX(id) \r\n" + "from orders;";

	public static final String insert2 = "INSERT INTO ordersitem (order_id,product_id,quantity) \r\n" + "values\r\n"
			+ "(?,?,?);";
	public static final String query2 = "select * \r\n" + "from product\r\n" + "where id=?";
	public static final String edit = "UPDATE product \r\n" + "SET \r\n" + "quantity=?" + "\r\n" + "WHERE\r\n"
			+ "    id = ?;";
	public static final String createBill = "select c.namee,c.age,c.phone,product.namee,product.quantity,product.price\r\n"
			+ "from product, clients c\r\n" + "INNER JOIN orders o\r\n" + "    on c.id=o.client_id\r\n"
			+ "INNER JOIN ordersitem oo\r\n" + "    on o.id = oo.order_id \r\n" + "where oo.product_id=product.id;";

	public GUI2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(3, 2));
		setSize(300, 125);
		add(l1);
		add(t1);
		add(l2);
		add(t2);
		add(l3);
		add(t3);
		add(bb);
		add(bbb);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(4, 1));
		setVisible(true);

		bb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int cantitate = 0;
				int client = Integer.parseInt(t1.getText());
				int product = Integer.parseInt(t2.getText());
				int quantity = Integer.parseInt(t3.getText());
				Connection con = ConnectionFactory.getConnection();
				PreparedStatement fd = null;
				ResultSet rs = null;
				try {
					fd = con.prepareStatement(query2);
					fd.setInt(1, product);
					rs = fd.executeQuery();
					rs.next();
					rs.getInt("id");
					rs.getString("namee");
					cantitate = rs.getInt("quantity");

				} catch (SQLException ee) {
					ee.printStackTrace();

				}
				if (cantitate < quantity)
					JOptionPane.showMessageDialog(null, "UNDER STOCK!\r\nPlease come back later!","Warning",JOptionPane.WARNING_MESSAGE);
				else {

					try {
						fd = con.prepareStatement(insert1);
						fd.setInt(1, client);
						fd.executeUpdate();
						fd = con.prepareStatement(query1);
						rs = fd.executeQuery();
						rs.next();
						int idMax = rs.getInt("MAX(id)");
						fd = con.prepareStatement(insert2);
						fd.setInt(1, idMax);
						fd.setInt(2, product);
						fd.setInt(3, quantity);
						fd.executeUpdate();
						int newQuantity = cantitate - quantity;
						fd = con.prepareStatement(edit);
						fd.setInt(1, newQuantity);
						fd.setInt(2, product);
						fd.executeUpdate();
						rs.close();
						fd.close();
						con.close();
						JOptionPane.showMessageDialog(null, "SUCCES!");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

			}
		});
		bbb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LOGG.setupLogger();
				Connection con = ConnectionFactory.getConnection();
				PreparedStatement fd = null;
				ResultSet rs = null;
				try {
					fd = con.prepareStatement(createBill);
					rs = fd.executeQuery();

					while (rs.next()) {
						String numeClient = rs.getString("namee");
						String varstaClient = rs.getString("age");
						String telClient = rs.getString("phone");
						String numeProdus = rs.getString("product.namee");
						int nr = rs.getInt("quantity");
						int price = rs.getInt("product.price");
						int total = nr * price;
						LOGG.LOGGER.log(Level.INFO,
								"\r\nBill to | Description | Quantity | Price | Total \r\nAge\r\nPhone\r\n" + numeClient
										+ " | " + numeProdus + " | " + nr + " | " + price + " | " + total + "\r\n"
										+ varstaClient + "\r\n" + telClient + "\r\n");

					}

				} catch (SQLException ee) {
					ee.printStackTrace();

				}

			}
		});
	}

}
