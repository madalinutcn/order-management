package presentation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import businessLayer.ClientDAO;
import model.Client;

/**
 * In aceasta clasa este realizata interfata pentru operatiile pe clienti +
 * afisarea tabelului cu clientii existenti.
 */

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	JTable table;
	JScrollPane jpg;
	TableModel model;
	JButton addButton;
	JButton editButton;
	JButton deleteButton;
	JButton viewClients;
	Object rows[][];
	String columns[] = { "ID", "Name", "Age", "Phone" };
	int i = 0;

	JTable createTable(List<Client> objects) {
		JTable tabela;
		this.i = objects.size();
		rows = new Object[i][i];
		for (int j = 0; j < i; j++) {
			rows[j][0] = objects.get(j).getId();
			rows[j][1] = objects.get(j).getName();
			rows[j][2] = objects.get(j).getAge();
			rows[j][3] = objects.get(j).getPhone();

		}
		model = new DefaultTableModel(rows, columns);
		tabela = new JTable(model);
		return tabela;
	}

	public GUI() {
		ClientDAO CLIENTUL = new ClientDAO();
		List<Client> clients = CLIENTUL.formClient();

		table = createTable(clients);
		jpg = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		addButton = new JButton("Add client");
		editButton = new JButton("Edit client");
		deleteButton = new JButton("Delete client");
		viewClients = new JButton("View clients");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		setSize(500, 100);
		add(addButton);
		add(editButton);
		add(deleteButton);
		add(viewClients);
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JLabel l1 = new JLabel("Name:");
				JLabel l2 = new JLabel("Age:");
				JLabel l3 = new JLabel("Phone:");
				JTextField t1 = new JTextField("");
				JTextField t2 = new JTextField("");
				JTextField t3 = new JTextField("");
				JButton bb = new JButton("OK");
				JPanel panel = new JPanel();
				JFrame frame = new JFrame("Adaugare client");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(300, 150);
				panel.setLayout(new GridLayout(4, 2));
				panel.setSize(200, 150);
				panel.add(l1);
				panel.add(t1);
				panel.add(l2);
				panel.add(t2);
				panel.add(l3);
				panel.add(t3);
				panel.add(bb);
				bb.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JFrame frame1 = new JFrame("Tabelul dupa adaugarea clientului");
						setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						panel.setLayout(new BorderLayout());
						frame1.setSize(480, 480);
						JPanel panel1 = new JPanel();
						String nume = t1.getText();
						int age = Integer.parseInt(t2.getText());
						String phone = t3.getText();
						ClientDAO.addClient(nume, age, phone);
						ClientDAO Cl = new ClientDAO();
						List<Client> clients = Cl.formClient();
						table = createTable(clients);
						jpg = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
								JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
						panel1.add(jpg, BorderLayout.CENTER);
						frame1.setContentPane(panel1);
						frame1.setVisible(true);

					}
				});
				frame.setContentPane(panel);
				frame.setVisible(true);

			}
		});
		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JLabel l1 = new JLabel("Name:");
				JLabel l2 = new JLabel("Age:");
				JLabel l3 = new JLabel("Phone:");
				JLabel l4 = new JLabel("ID");
				JButton bb = new JButton("OK");
				JTextField t1 = new JTextField("");
				JTextField t2 = new JTextField("");
				JTextField t3 = new JTextField("");
				JTextField t4 = new JTextField("");
				JPanel panel = new JPanel();
				JFrame frame = new JFrame("Editare client");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(300, 140);
				panel.setLayout(new GridLayout(5, 2));
				panel.setSize(200, 100);
				panel.add(l1);
				panel.add(t1);
				panel.add(l2);
				panel.add(t2);
				panel.add(l3);
				panel.add(t3);
				panel.add(l4);
				panel.add(t4);
				panel.add(bb);
				bb.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JFrame frame1 = new JFrame("Tabelul dupa editarea clientului");
						setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						panel.setLayout(new BorderLayout());
						frame1.setSize(480, 480);
						JPanel panel1 = new JPanel();
						String nume = t1.getText();
						int age = Integer.parseInt(t2.getText());
						String phone = t3.getText();
						int id = Integer.parseInt(t4.getText());
						ClientDAO.editClient(nume, age, phone, id);
						ClientDAO Cl = new ClientDAO();
						List<Client> clients = Cl.formClient();
						table = createTable(clients);
						jpg = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
								JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
						panel1.add(jpg, BorderLayout.CENTER);
						frame1.setContentPane(panel1);
						frame1.setVisible(true);

					}
				});
				frame.setContentPane(panel);
				frame.setVisible(true);
			}
		});
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JLabel l1 = new JLabel("ID:");
				JTextField t1 = new JTextField("");
				JButton bb = new JButton("OK");
				JPanel panel = new JPanel();
				JFrame frame = new JFrame("Stergere client");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(300, 120);
				panel.setLayout(new GridLayout(4, 2));
				panel.setSize(200, 100);
				panel.add(l1);
				panel.add(t1);
				panel.add(bb);
				bb.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JFrame frame1 = new JFrame("Tabelul dupa editarea clientului");
						setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						panel.setLayout(new BorderLayout());
						frame1.setSize(480, 480);
						JPanel panel1 = new JPanel();
						int id = Integer.parseInt(t1.getText());
						ClientDAO.deleteClient(id);
						ClientDAO Cl = new ClientDAO();
						List<Client> clients = Cl.formClient();
						table = createTable(clients);
						jpg = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
								JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
						panel1.add(jpg, BorderLayout.CENTER);
						frame1.setContentPane(panel1);
						frame1.setVisible(true);

					}
				});
				frame.setContentPane(panel);
				frame.setVisible(true);
			}
		});
		viewClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frame1 = new JFrame("Afisarea clientilor");
				JPanel panel = new JPanel();
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				panel.setLayout(new BorderLayout());
				frame1.setSize(480, 480);
				JPanel panel1 = new JPanel();
				ClientDAO Cl = new ClientDAO();
				List<Client> clients = Cl.formClient();
				table = createTable(clients);
				jpg = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
						JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
				panel1.add(jpg, BorderLayout.CENTER);
				frame1.setContentPane(panel1);
				frame1.setVisible(true);

					}
				});
			
		setVisible(true);
	}

}
