package businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dataAccesLayer.ConnectionFactory;
import model.Product;

/**
 * In acesta clasa se realizeaza adaugarea, editarea sau eliminarea unui produs
 * existent. Totodata in metoda formClient se creeaza un obiect Product
 * corespunzator fiecarui produs din baza de date.
 */

public class ProductDAO {

	public static final String addStatement = "INSERT INTO product (namee,quantity,price)\r\n" + "VALUES\r\n"
			+ "(?,?,?);";
	public static final String editStatement = "UPDATE product \r\n" + "SET \r\n" + "    namee = ?,\r\n"
			+ "quantity = ?,\r\n" + "price = ?\r\n" + "WHERE\r\n" + "    id = ?;";

	public static final String deleteStatement = "DELETE FROM product \r\n" + "WHERE\r\n" + "id = ?;";
	public static final String query = "SELECT * FROM product";

	public static void addProduct(String a, int b, int c) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		try {
			fd = con.prepareStatement(addStatement);
			fd.setString(1, a);
			fd.setInt(2, b);
			fd.setInt(3, c);
			fd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public List<Product> formProduct() {

		List<Product> products = new ArrayList<Product>();
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		ResultSet rs = null;
		try {
			fd = con.prepareStatement(query);
			rs = fd.executeQuery();
			while (rs.next()) {
				Product c = new Product(0, "0", 0, 0);
				c.setId(rs.getInt("id"));
				c.setName(rs.getString("namee"));
				c.setQuantity(rs.getInt("quantity"));
				c.setPrice(rs.getInt("price"));
				Reflection.retrieveProperties(c);
				products.add(c);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return products;
	}

	public static void editProduct(String a, int b, int c, int d) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		try {
			fd = con.prepareStatement(editStatement);
			fd.setString(1, a);
			fd.setInt(2, b);
			fd.setInt(3, c);
			fd.setInt(4, d);
			fd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void deleteProduct(int a) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		try {
			fd = con.prepareStatement(deleteStatement);
			fd.setInt(1, a);
			fd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
