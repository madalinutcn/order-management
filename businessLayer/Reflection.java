package businessLayer;

import java.lang.reflect.Field;

/**
 * Aceasta clasa va fi folosita in prodesul de creare al tabelului clientilor si
 * produselor existente in baza de date.
 */

public class Reflection {

	public static void retrieveProperties(Object o) {
		for (Field field : o.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				field.get(o);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
