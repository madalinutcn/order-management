package businessLayer;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Aceasta clasa va fi folosita pentru crearea unei facturi pentru fiecare
 * client care a plasat o comanda pentru un anumit produs.
 */

public class LOGG {
	public static final Logger LOGGER = Logger.getLogger(LOGG.class.getName());
	static FileHandler fh;

	public static void setupLogger() {
		LOGGER.setLevel(Level.ALL);
		try {
			FileHandler fhandler = new FileHandler("bills.log");
			SimpleFormatter sformatter = new SimpleFormatter();
			fhandler.setFormatter(sformatter);
			LOGGER.addHandler(fhandler);

		} catch (IOException ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		} catch (SecurityException ex) {
			LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
		}
	}

}
