package businessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import dataAccesLayer.ConnectionFactory;
import model.*;

/**
 * In acesta clasa se realizeaza adaugarea, editarea sau eliminarea unui client
 * existent. Totodata in metoda formClient se creeaza un obiect Client
 * corespunzator fiecarui client din baza de date.
 */

public class ClientDAO {
	public static final String addStatement = "INSERT INTO clients (namee,age,phone)\r\n" + "VALUES\r\n" + "(?, ?, ?);";
	public static final String editStatement = "UPDATE clients \r\n" + "SET \r\n" + "    namee = ?,\r\n" + "age=?,\r\n"
			+ "phone=?\r\n" + "\r\n" + "WHERE\r\n" + "    id = ?;";

	public static final String deleteStatement = "DELETE FROM clients \r\n" + "WHERE\r\n" + "id = ?;";
	public static final String query = "SELECT * FROM clients";

	public static void addClient(String a, int b, String c) {

		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		try {
			fd = con.prepareStatement(addStatement);
			fd.setString(1, a);
			fd.setInt(2, b);
			fd.setString(3, c);
			fd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public List<Client> formClient() {

		List<Client> clients = new ArrayList<Client>();
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		ResultSet rs = null;
		try {
			fd = con.prepareStatement(query);
			rs = fd.executeQuery();
			while (rs.next()) {
				Client c = new Client(0, "0", 0, "0");
				c.setId(rs.getInt("id"));
				c.setName(rs.getString("namee"));
				c.setAge(rs.getInt("age"));
				c.setPhone(rs.getString("phone"));
				Reflection.retrieveProperties(c);
				clients.add(c);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return clients;
	}

	public static void editClient(String a, int b, String c, int d) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		try {
			fd = con.prepareStatement(editStatement);
			fd.setString(1, a);
			fd.setInt(2, b);
			fd.setString(3, c);
			fd.setInt(4, d);
			fd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void deleteClient(int a) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		try {
			fd = con.prepareStatement(deleteStatement);
			fd.setInt(1, a);
			fd.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fd.close();
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
