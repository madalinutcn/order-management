package dataAccesLayer;

import java.sql.*;

/**
 * Aici se realizeaza conexiunea la baza de date in care avem stocate toate
 * informatiile referitoare la clienti,produse si comenzi.
 */
public class ConnectionFactory {

	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/ordermanagement";
	private static final String USER = "root";
	private static final String PASS = "studentlacluj";
	public static final String da = "select * from clients";

	public ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		Connection con = null;
		try {
			con = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}

	public static void close(Connection con) {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void close(Statement st) {
		try {
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void close(ResultSet rs) {
		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void afis() {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement fd = null;
		ResultSet rs = null;

		try {
			fd = con.prepareStatement(da);
			rs = fd.executeQuery();
			rs.next();
			String nume = rs.getString("namee");
			System.out.println(nume);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs.close();
			fd.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
